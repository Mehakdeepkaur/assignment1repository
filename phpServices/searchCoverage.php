<?php 

header('Access-Controle-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept'); 
header('Content-Type: application/json');
header("Access-Controle-Allow-Methods: POST,GET, OPTIONS, PUT");


	try 
	{
	    $req= json_decode( file_get_contents( 'php://input' ));
	    $name = $req->field1;

		require "../db/config.php";

		$connection = new PDO ( $dsn );

		$sql = "SELECT * FROM Coverage WHERE Coverage_Name = :name";

		$statement = $connection->prepare($sql);
		$statement->bindParam(':name', $name, PDO::PARAM_STR);
		$statement->execute();

		$result = $statement->fetchAll();

		$myarr = array();
		
		foreach ($result as $row) {
			$myarr[] = array("ID" => $row["ID"], "NAME" => $row["Coverage_Name"], "COST" => $row["Cost"]);
		}

		echo json_encode($myarr);
	}
	
	catch(PDOException $error) 
	{
		echo $sql . "<br>" . $error->getMessage();
	}

?>